// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.21.9
// source: proto/master/catalog/passenger.proto

package catalog

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// PassengerServiceClient is the client API for PassengerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PassengerServiceClient interface {
	CreatePassenger(ctx context.Context, in *CreatePassengerRequest, opts ...grpc.CallOption) (*Passenger, error)
	UpdatePassenger(ctx context.Context, in *UpdatePassengerRequest, opts ...grpc.CallOption) (*Passenger, error)
	DeletePassenger(ctx context.Context, in *PassengerIdentifierRequest, opts ...grpc.CallOption) (*DeletePassengerResponse, error)
	GetPassengerByID(ctx context.Context, in *PassengerIdentifierRequest, opts ...grpc.CallOption) (*Passenger, error)
	GetListPassenger(ctx context.Context, in *PassengerFilterQuery, opts ...grpc.CallOption) (*ListPassenger, error)
}

type passengerServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPassengerServiceClient(cc grpc.ClientConnInterface) PassengerServiceClient {
	return &passengerServiceClient{cc}
}

func (c *passengerServiceClient) CreatePassenger(ctx context.Context, in *CreatePassengerRequest, opts ...grpc.CallOption) (*Passenger, error) {
	out := new(Passenger)
	err := c.cc.Invoke(ctx, "/master.catalog.PassengerService/CreatePassenger", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerServiceClient) UpdatePassenger(ctx context.Context, in *UpdatePassengerRequest, opts ...grpc.CallOption) (*Passenger, error) {
	out := new(Passenger)
	err := c.cc.Invoke(ctx, "/master.catalog.PassengerService/UpdatePassenger", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerServiceClient) DeletePassenger(ctx context.Context, in *PassengerIdentifierRequest, opts ...grpc.CallOption) (*DeletePassengerResponse, error) {
	out := new(DeletePassengerResponse)
	err := c.cc.Invoke(ctx, "/master.catalog.PassengerService/DeletePassenger", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerServiceClient) GetPassengerByID(ctx context.Context, in *PassengerIdentifierRequest, opts ...grpc.CallOption) (*Passenger, error) {
	out := new(Passenger)
	err := c.cc.Invoke(ctx, "/master.catalog.PassengerService/GetPassengerByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *passengerServiceClient) GetListPassenger(ctx context.Context, in *PassengerFilterQuery, opts ...grpc.CallOption) (*ListPassenger, error) {
	out := new(ListPassenger)
	err := c.cc.Invoke(ctx, "/master.catalog.PassengerService/GetListPassenger", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PassengerServiceServer is the server API for PassengerService service.
// All implementations must embed UnimplementedPassengerServiceServer
// for forward compatibility
type PassengerServiceServer interface {
	CreatePassenger(context.Context, *CreatePassengerRequest) (*Passenger, error)
	UpdatePassenger(context.Context, *UpdatePassengerRequest) (*Passenger, error)
	DeletePassenger(context.Context, *PassengerIdentifierRequest) (*DeletePassengerResponse, error)
	GetPassengerByID(context.Context, *PassengerIdentifierRequest) (*Passenger, error)
	GetListPassenger(context.Context, *PassengerFilterQuery) (*ListPassenger, error)
	mustEmbedUnimplementedPassengerServiceServer()
}

// UnimplementedPassengerServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPassengerServiceServer struct {
}

func (UnimplementedPassengerServiceServer) CreatePassenger(context.Context, *CreatePassengerRequest) (*Passenger, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreatePassenger not implemented")
}
func (UnimplementedPassengerServiceServer) UpdatePassenger(context.Context, *UpdatePassengerRequest) (*Passenger, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePassenger not implemented")
}
func (UnimplementedPassengerServiceServer) DeletePassenger(context.Context, *PassengerIdentifierRequest) (*DeletePassengerResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeletePassenger not implemented")
}
func (UnimplementedPassengerServiceServer) GetPassengerByID(context.Context, *PassengerIdentifierRequest) (*Passenger, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPassengerByID not implemented")
}
func (UnimplementedPassengerServiceServer) GetListPassenger(context.Context, *PassengerFilterQuery) (*ListPassenger, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetListPassenger not implemented")
}
func (UnimplementedPassengerServiceServer) mustEmbedUnimplementedPassengerServiceServer() {}

// UnsafePassengerServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PassengerServiceServer will
// result in compilation errors.
type UnsafePassengerServiceServer interface {
	mustEmbedUnimplementedPassengerServiceServer()
}

func RegisterPassengerServiceServer(s grpc.ServiceRegistrar, srv PassengerServiceServer) {
	s.RegisterService(&PassengerService_ServiceDesc, srv)
}

func _PassengerService_CreatePassenger_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreatePassengerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerServiceServer).CreatePassenger(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/master.catalog.PassengerService/CreatePassenger",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerServiceServer).CreatePassenger(ctx, req.(*CreatePassengerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerService_UpdatePassenger_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdatePassengerRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerServiceServer).UpdatePassenger(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/master.catalog.PassengerService/UpdatePassenger",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerServiceServer).UpdatePassenger(ctx, req.(*UpdatePassengerRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerService_DeletePassenger_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PassengerIdentifierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerServiceServer).DeletePassenger(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/master.catalog.PassengerService/DeletePassenger",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerServiceServer).DeletePassenger(ctx, req.(*PassengerIdentifierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerService_GetPassengerByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PassengerIdentifierRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerServiceServer).GetPassengerByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/master.catalog.PassengerService/GetPassengerByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerServiceServer).GetPassengerByID(ctx, req.(*PassengerIdentifierRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _PassengerService_GetListPassenger_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(PassengerFilterQuery)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PassengerServiceServer).GetListPassenger(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/master.catalog.PassengerService/GetListPassenger",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PassengerServiceServer).GetListPassenger(ctx, req.(*PassengerFilterQuery))
	}
	return interceptor(ctx, in, info, handler)
}

// PassengerService_ServiceDesc is the grpc.ServiceDesc for PassengerService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var PassengerService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "master.catalog.PassengerService",
	HandlerType: (*PassengerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreatePassenger",
			Handler:    _PassengerService_CreatePassenger_Handler,
		},
		{
			MethodName: "UpdatePassenger",
			Handler:    _PassengerService_UpdatePassenger_Handler,
		},
		{
			MethodName: "DeletePassenger",
			Handler:    _PassengerService_DeletePassenger_Handler,
		},
		{
			MethodName: "GetPassengerByID",
			Handler:    _PassengerService_GetPassengerByID_Handler,
		},
		{
			MethodName: "GetListPassenger",
			Handler:    _PassengerService_GetListPassenger_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto/master/catalog/passenger.proto",
}
