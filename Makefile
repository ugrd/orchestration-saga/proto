CHANGED_PROTO_FILES := $(shell git diff --name-only -- '*.proto')
CHANGED_PROTOBUF_FILES := $(shell git diff --name-only -- 'protobuf/master/*/*.go')
UNTRACKED_PROTO_FILES := $(shell git ls-files --others --exclude-standard -- '*.proto')
UNTRACKED_PROTOBUF_FILES := $(shell git ls-files --others --exclude-standard -- 'protobuf/master/*/*.go')

protoc:
	for f in ${CHANGED_PROTO_FILES}; do \
		protoc --go_out=. $$f; \
		protoc --go-grpc_out=. $$f; \
		echo compiled: $$f; \
	done

	for f in ${CHANGED_PROTOBUF_FILES}; do \
		protoc-go-inject-tag -input=$$f; \
        echo injected tags: $$f; \
	done

	for f in ${UNTRACKED_PROTO_FILES}; do \
		protoc --go_out=. $$f; \
		protoc --go-grpc_out=. $$f; \
		echo compiled: $$f; \
	done

	for f in ${UNTRACKED_PROTOBUF_FILES}; do \
		protoc-go-inject-tag -input=$$f; \
        echo injected tags: $$f; \
	done